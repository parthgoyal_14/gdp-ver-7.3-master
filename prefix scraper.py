import csv

prefix = []
def returnPrefix(str):
    temp = str.split(" ", 3)
    pref=""
    if (len(temp)>3):
        pref = temp[0] + " " + temp[1] + " " + temp[2]
    return pref

with open(r'database/database_final.csv', 'r', encoding='utf-8') as file:
    csv_file_reader = csv.reader(file)
    for row in csv_file_reader:
        temp = returnPrefix(row[1]).lower()
        if(temp==""):
            continue
        elif(temp in prefix):
            continue
        else:
            prefix.append(temp)

print(prefix)