from django.db import models

class Projects(models.Model):
   project_name = models.CharField(max_length=300)
   createdAt = models.DateTimeField(auto_now=True)
   class Meta:
      db_table = "projects"

class UrsDescriptions(models.Model):
   ursid = models.CharField(max_length=300, default=None)
   description = models.CharField(max_length = 10000)
   project_id = models.ForeignKey(Projects, default=1, on_delete=models.CASCADE, db_index=True)
   class Meta:
      db_table = "ursdescription"

class UrsTempTable(models.Model):
   ursid = models.CharField(max_length=300, default=None)
   description = models.CharField(max_length=10000)
   project_id = models.ForeignKey(Projects, default=1, on_delete=models.CASCADE, db_index=True)
   class Meta:
      db_table = "urstemptable"