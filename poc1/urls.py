from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views
urlpatterns = [
    path('', views.base, name='hi'),
    path('upload_urs/', views.upload_urs, name='URS'),
    path('upload_azure/', views.upload_azure, name='Azure'),
    path('add_project/', views.add_project, name='Add Project'),
    path('add_document/', views.add_document, name='Add Document'),
    path('upload_frs/', views.upload_frs, name='FRS'),
    path('upload_ds/', views.upload_ds, name='DS'),
    path('about_us/', views.about_us, name='About Us'),
    path('contact_us/', views.contact_us, name='Contact Us'),
    path('coming_soon/', views.coming_soon, name='Coming Soon'),
    path('help/', views.help, name='Help'),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)