# imports
from poc1.models import Projects, UrsDescriptions, UrsTempTable
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage, Storage
from django.contrib import messages
from docx import Document
import nltk
import csv
import gensim
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import os
import pandas as pd
import xlsxwriter
import language_check
from collections import Counter
import io
import zipfile
import xml.etree.ElementTree
import warnings
import grammar_check

warnings.filterwarnings('ignore')

tool = language_check.LanguageTool('en-GB')

# Old logic for reading tables
'''def read_docx_tables(filename, tab_id=None, **kwargs):
    def read_docx_tab(tab, **kwargs):
        vf = io.StringIO()
        writer = csv.writer(vf)
        for row in tab.rows:
            writer.writerow(cell.text for cell in row.cells) 
        vf.seek(0)
        return pd.read_csv(vf, **kwargs, na_filter=False)
    doc = Document(filename)
    if tab_id is None:
        return [read_docx_tab(tab, **kwargs) for tab in doc.tables]
    else:
        try:
            return read_docx_tab(doc.tables[tab_id], **kwargs)
        except IndexError:
            print('Error: specified [tab_id]: {} does not exist.'.format(tab_id))
            raise'''


def add_project(request):
    context = {}
    global project_name
    existing_projects = list(Projects.objects.values_list('project_name', flat=True).distinct())
    if request.method == 'POST' and 'check_projects' in request.POST:
        project_name = request.POST.get('project_name', None)
        if project_name in existing_projects:
            messages.warning(request, 'Project already exists!')
        else:
            project = Projects(project_name=project_name)
            project.save()
            messages.info(request, 'Project Successfully added!')

    return render(request, 'add_project.html', context)


def add_document(request):
    context = {}
    context['project_names'] = [(x.project_name, str(x.createdAt).rsplit(" ", 1)[0]) for x in
                                list(Projects.objects.order_by('project_name'))]
    global project_name
    existing_projects = list(Projects.objects.values_list('project_name', flat=True).distinct())

    if request.method == 'POST' and 'add_document' in request.POST:
        project_name = request.POST.get('project_name', None)
        table_identifier = request.POST.get('table_identifier', None)
        uploaded_urs_file = request.FILES['URS document']

        fs = FileSystemStorage(location="media/" + "Projects" + "/", base_url="/media/" + 'URS' + "/")
        saved_name = fs.save(uploaded_urs_file.name, uploaded_urs_file)
        messages.success(request, "File uploaded successfully")

        file_path = "media/" + 'Projects' + "/" + saved_name
        create_backup(project_name)
        delete_original(project_name)
        write_docx_to_sql(file_path, 'URS', table_identifier, project_name)
        update_timestamp(project_name)
        delete_backup(project_name)

        # delete project word file after data is transferred to SQL
        fs.delete(uploaded_urs_file.name)

    return render(request, 'add_document.html', context)

def create_backup(projectName):
    project = Projects.objects.get(project_name=projectName)
    UrsTempTable.objects.bulk_create(UrsDescriptions.objects.filter(project_id=project))

def delete_original(projectName):
    project = Projects.objects.get(project_name=projectName)
    try:
        descriptions = UrsDescriptions.objects.filter(project_id=project)
    except UrsDescriptions.DoesNotExist:
        descriptions = None
    if descriptions != None:
        UrsDescriptions.objects.filter(project_id=project).delete()

def delete_backup(projectName):
    project = Projects.objects.get(project_name=projectName)
    try:
        tempDescriptions = UrsTempTable.objects.filter(project_id=project)
    except UrsTempTable.DoesNotExist:
        tempDescriptions = None
    if tempDescriptions != None:
        UrsTempTable.objects.filter(project_id=project).delete()

def update_timestamp(projectName):
    project = Projects.objects.get(project_name=projectName)
    project.save()

def write_docx_to_sql(file_path, doc_type, table_pick_criteria, projectName):
    project = Projects.objects.get(project_name=projectName)

    import zipfile
    import xml.etree.ElementTree as ET

    WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
    PARA = WORD_NAMESPACE + 'p'
    TEXT = WORD_NAMESPACE + 't'
    TABLE = WORD_NAMESPACE + 'tbl'
    ROW = WORD_NAMESPACE + 'tr'
    CELL = WORD_NAMESPACE + 'tc'

    with zipfile.ZipFile(file_path) as docx:
        tree = xml.etree.ElementTree.XML(docx.read('word/document.xml'))

    for table in tree.iter(TABLE):
        b = []
        first_header = ''
        row_count = 1
        for row in table.iter(ROW):
            a = []

            for cell in row.iter(CELL):
                text = ''.join(node.text for node in cell.iter(TEXT))
                if (first_header == ''):
                    first_header = text
                    break
                elif (first_header.strip() == table_pick_criteria):
                    a.append(text)
                else:
                    break
            if ((doc_type != 'ds' and first_header.strip() == table_pick_criteria) and row_count != 1):
                descr = UrsDescriptions(
                    ursid=a[0],
                    description=a[1],
                    project_id=project
                )
                descr.save()
                b.append(a)

            row_count += 1


def remove_whitespace(string):
    return string.replace(" ", "")


def make_dataframes(file_path, doc_type, table_pick_criteria):
    import zipfile
    import xml.etree.ElementTree as ET
    import csv
    import pandas as pd

    WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
    PARA = WORD_NAMESPACE + 'p'
    TEXT = WORD_NAMESPACE + 't'
    TABLE = WORD_NAMESPACE + 'tbl'
    ROW = WORD_NAMESPACE + 'tr'
    CELL = WORD_NAMESPACE + 'tc'

    with zipfile.ZipFile(file_path) as docx:
        tree = xml.etree.ElementTree.XML(docx.read('word/document.xml'))

    if doc_type == 'urs':
        headers = ['Req. Num.', 'Req. Desc.', 'Risk Determination', 'Risk Level']
    elif doc_type == 'frs':
        headers = ['FRS Identifier', 'Req. Desc.', 'URS Identifier']
    else:
        headers = ['CS Identifier', 'Configuration Design Specification', 'URS/FRS Identifier']

    dfs = []
    for table in tree.iter(TABLE):
        b = []
        first_header = ''
        row_count = 1
        for row in table.iter(ROW):
            a = []

            for cell in row.iter(CELL):
                text = ''.join(node.text for node in cell.iter(TEXT))
                if (first_header == ''):
                    first_header = text
                    break
                elif (doc_type != 'ds' and first_header.strip() == table_pick_criteria) or (
                        doc_type == 'ds' and first_header.strip().endswith(table_pick_criteria)):
                    a.append(text)
                else:
                    break
            if (((doc_type != 'ds' and first_header.strip() == table_pick_criteria) or (
                    doc_type == 'ds' and first_header.strip().endswith(table_pick_criteria))) and row_count != 1):
                b.append(a)

            row_count += 1
        if (doc_type != 'ds' and first_header.strip() == table_pick_criteria) or (
                doc_type == 'ds' and first_header.strip().endswith(table_pick_criteria)):
            df = pd.DataFrame(b, columns=headers)
            dfs.append(df)
    return (dfs)


# Color legends for writing to excel file cells
def color_negative_red(val, df, doc_type):
    def coloring(v):
        if str(v).strip() in val[0]:
            color = '#E74C3C'
        elif v in val[1]:
            color = '#A569BD'
        elif v in val[3]:
            color = '#BDF43F'
        elif v in val[2]:
            color = '#5DADE2'
        # elif v in val[4]:
        #    color = '#F4D03F'
        else:
            color = 'white'
        return 'background-color: %s' % color

    return coloring


def applycolor(coloring_list, doc_type):
    df = pd.read_excel("media/" + doc_type + "/" + name + '.xlsx', na_filter=False)
    dat = {'Legend': []}
    dflegend = pd.DataFrame(dat, columns=['Legend'])

    snew = df.style. \
        applymap(color_negative_red(coloring_list, df, doc_type))

    writer = pd.ExcelWriter("media/" + doc_type + "/" + name + '.xlsx', engine='xlsxwriter')
    dflegend.to_excel(writer, sheet_name='Legend', startrow=0, startcol=0, index=None)
    worksheet = writer.sheets['Legend']
    # Insert legend image
    worksheet.insert_image('A2', 'static/assets/legend.PNG')
    snew.to_excel(writer, sheet_name='Report', startcol=0, index=None)
    writer.save()

    # snew.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=None)


# view for home page
def base(request):
    return render(request, 'base.html')


# View for About US page
def about_us(request):
    return render(request, 'about_us.html')


# View for Contact Us page
def contact_us(request):
    return render(request, 'contact_us.html')


# View for Help page
def help(request):
    return render(request, 'help.html')


# View For Coming Soon Page
def coming_soon(request):
    return render(request, 'coming_soon.html')


def create_excel_for_create_urs(matching_results, dl_result, context):
    with open("media/" + 'report_' + azure_name + '.csv', 'w', encoding='utf-8', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Feature Title', 'Existing URS Suggestion', 'New URS Suggestion'])
        for i in range(len(matching_results)):
            writer.writerow([matching_results[i][0], matching_results[i][1], dl_result[i]])
    fs = FileSystemStorage()
    context['azure_full_report_url'] = fs.url('report_' + azure_name + '.csv')


# View for uploading azure file and then creating urs from it
def upload_azure(request):
    context = {}
    global azure_name
    global projectName
    train_data = ""
    # context['project_names'] = list(Projects.objects.values_list('project_name', flat=True).distinct())
    context['project_names'] = [(x.project_name, str(x.createdAt).rsplit(" ", 1)[0]) for x in
                                list(Projects.objects.order_by('project_name'))]

    # Post value upload_azure for form in base.html
    if request.method == 'POST' and 'upload_azure' in request.POST:
        projectName = request.POST.get('project_name', None)
        uploaded_azure_file = request.FILES['Azure document']
        azure_name = check_csv(uploaded_azure_file, request, context)
        if azure_name == "null":
            return render(request, 'upload_azure.html', context)
        request.session['azure_name'] = azure_name

    # Post value create_urs after uploading azure file
    if request.method == 'POST' and 'create_urs' in request.POST:
        project = Projects.objects.get(project_name=projectName)
        projectId = project.id
        projects_with_file_attached = list(UrsDescriptions.objects.values_list('project_id', flat=True).distinct())

        if projectId not in projects_with_file_attached:
            messages.error(request, projectName + " does not have URS document attached to it! please add document to the project first! ")
            return render(request, 'upload_azure.html', context)

        matching_results = []
        azure_name = request.session['azure_name']
        stop_words = set(stopwords.words('english'))
        dataset = []
        idlist = []
        gen_docs = []

        idlist = [x.ursid for x in UrsDescriptions.objects.filter(project_id=projectId)]
        dataset = [x.description for x in UrsDescriptions.objects.filter(project_id=projectId)]

        with open(r'database/database_final.csv', 'r', encoding='utf-8') as file:
            csv_file_reader = csv.reader(file)
            for row in csv_file_reader:
                train_data = train_data + '\n' + row[1]
                # dataset.append(row[1])
                # idlist.append(row[0])

        for text in dataset:
            word_tokens = word_tokenize(text)
            filtered_sentence = [w for w in word_tokens if not w.lower() in stop_words and w.isalpha()]
            gen_docs.append(filtered_sentence)

        dictionary = gensim.corpora.Dictionary(gen_docs)

        corpus = [dictionary.doc2bow(gen_doc) for gen_doc in gen_docs]

        import numpy as np
        tf_idf = gensim.models.TfidfModel(corpus)
        # for doc in tf_idf[corpus]:
        # print([[dictionary[id], np.around(freq, decimals=2)] for id, freq in doc])

        sims = gensim.similarities.Similarity(r'poc1', tf_idf[corpus],
                                              num_features=len(dictionary))

        query = []
        with open('media/' + azure_name, 'r', newline='') as file:
            csv_file_reader = csv.reader(file)
            for row in csv_file_reader:
                temp = row[2]
                if temp != 'Title':
                    query.append(temp)

        for line in query:
            query_doc = [w.lower() for w in word_tokenize(line)]
            query_doc_bow = dictionary.doc2bow(query_doc)
            # update an existing dictionary and create bag of words
            # perform a similarity query against the corpus
            query_doc_tf_idf = tf_idf[query_doc_bow]
            # print(document_number, document_similarity)
            # print('Comparing Result:', sims[query_doc_tf_idf].index(max(sims[query_doc_tf_idf])))
            x = (sims[query_doc_tf_idf]).tolist()
            y = x.copy()
            y.sort(reverse=True)
            if round(y[2] * 100, 2) == 0.0:
                idlist[x.index(y[2])] = "None"
                dataset[x.index(y[2])] = "None"
                if round(y[1] * 100, 2) == 0.0:
                    idlist[x.index(y[1])] = "None"
                    dataset[x.index(y[1])] = "None"
                    if round(y[0] * 100, 2) == 0.0:
                        idlist[x.index(y[0])] = "None"
                        dataset[x.index(y[0])] = "None"
            templist = ['Match Percentage: ' + str(round(y[0] * 100, 2)) + '% --> ' + idlist[x.index(y[0])] + "-"
                        + dataset[x.index(y[0])],
                        'Match Percentage: ' + str(round(y[1] * 100, 2)) + '% --> ' + idlist[x.index(y[1])] + "-"
                        + dataset[x.index(y[1])],
                        'Match Percentage: ' + str(round(y[2] * 100, 2)) + '% --> ' + idlist[x.index(y[2])] + "-"
                        + dataset[x.index(y[2])]]
            matching_results.append((line, templist))
        dl_result = DeepLearning(train_data, query)
        ##ram changes starts
        # queryresult = dl_result.split(" ")
        # newSentenceList = []
        # for word in querylist:
        # if word not in newSentenceList:
        # newSentenceList.append(word)
        # dl_result = ‘ ‘.join(newSentenceList)
        create_excel_for_create_urs(matching_results, dl_result, context)
        combined_result = zip(matching_results, dl_result)
        context['combined_result'] = combined_result
        # print(combined_result)
        return render(request, 'urs_created.html', context)
    return render(request, 'upload_azure.html', context)

# Function to check if input File is of csv format and store file in server if it of csv format
def check_csv(input_file_name, request, context):
    ext = os.path.splitext(input_file_name.name)[1]
    if ext != '.csv':
        print("entered if cond")
        messages.error(request, 'File is not CSV type')
        return "null"
    # csv file so save it in Media folder
    fs = FileSystemStorage()
    saved_name = fs.save(input_file_name.name, input_file_name)
    messages.success(request, "File uploaded successfully")
    # print(fs.url(saved_name))
    context['saved_url'] = fs.url(saved_name)
    return saved_name


# View for uploading doc file to check grammar and create a excel report
def upload_urs(request):
    doc_type = 'urs'
    context = {}
    global name
    # Check if uploaded file is docx/csv
    if request.method == 'POST' and 'upload' in request.POST:
        table_identifier = request.POST.get('table_identifier', None)
        uploaded_urs_file = request.FILES['URS document']
        name = check_csv_or_docx(uploaded_urs_file, request, context, doc_type)
        request.session['name'] = name

        ext = os.path.splitext(uploaded_urs_file.name)[1]
        # if file is docx
        if ext == ".docx":
            file_path = "media/" + doc_type + "/" + name
            write_docx_to_csv(file_path, request, doc_type, table_identifier)

        # if file is a csv type
        elif ext == ".csv":
            file_path = "media/" + name
            write_csv_to_csv(file_path)

    # Validation Rules
    if request.method == 'POST' and 'validate_urs' in request.POST:
        coloring_list = []
        name = request.session['name']
        empty_list = ['', 'nan', 'NaN', 'Nan', 'naN']
        unique_error_list = check_unique_errors(context, doc_type)
        desc_errors, original_but_incorrect_list = check_desc_errors(context, doc_type)
        identifier_error_list = check_id_errors(context, doc_type)

        coloring_list.append(empty_list)
        coloring_list.append(unique_error_list)
        coloring_list.append(desc_errors)
        coloring_list.append(original_but_incorrect_list)
        coloring_list.append(identifier_error_list)

        applycolor(coloring_list, doc_type)

        fs = FileSystemStorage(location="media/", base_url="/media/" + doc_type + "/")
        context['full_report_url'] = fs.url(name + '.xlsx')
        return render(request, 'validate_urs.html', context)

    return render(request, 'upload_urs.html', context)


# check if the uploaded document is of csv or docx format
def check_csv_or_docx(input_file_name, request, context, doc_type):
    ext = os.path.splitext(input_file_name.name)[1]
    valid_extensions = ['.csv', '.docx']
    if not ext in valid_extensions:
        messages.error(request, 'File is not CSV or DOCX type')
        if doc_type == 'urs':
            return render(request, 'upload_urs.html', context)
        elif doc_type == 'frs':
            return render(request, 'upload_frs.html', context)
        else:
            return render(request, 'upload_ds.html', context)
    fs = FileSystemStorage(location="media/" + doc_type + "/", base_url="/media/" + doc_type + "/")
    saved_name = fs.save(input_file_name.name, input_file_name)
    messages.success(request, "File uploaded successfully")
    # print(fs.url(saved_name))
    context['url'] = fs.url(saved_name)
    # print(saved_name)
    return saved_name


# If uploaded urs file is in docx format take the first two columns(requirement number and req description)
# of required tables in it and put it in csv file

def write_docx_to_csv(file_path, request, doc_type, table_identifier):
    document = Document(file_path)
    col1 = []
    col2 = []
    if doc_type != 'urs':
        col3 = []
    table_pick_criteria = table_identifier
    # if doc_type == 'urs':
    # table_pick_criteria = table_identifier
    # elif doc_type == 'frs':
    # table_pick_criteria = table_identifier
    # else:
    # table_pick_criteria = "Identifier"
    '''print('creating col1 and col2 list started')
    with open("media/" + doc_type + "/" + name + '.csv', 'w', encoding='utf-8', newline='') as file:
        writer = csv.writer(file)
        count = 1
        for table in document.tables:
            if (doc_type != 'ds' and table.rows[0].cells[0].text.strip() == table_pick_criteria) or (doc_type == 'ds' and table.rows[0].cells[0].text.strip().endswith(table_pick_criteria)):
                # using the tables first column name to
                # distinguish it as a urs/frs/ds table
                for row in table.rows[1:]:
                    if(row.cells[0].text == 'N/A'):
                        continue
                    else:
                        print(row.cells[0].text)
                        col1.append(row.cells[0].text)
                        col2.append(row.cells[1].text)
                        if doc_type == 'urs':
                            writer.writerow([row.cells[0].text, row.cells[1].text])
                        else:
                            col3.append(row.cells[2].text)
                            writer.writerow([row.cells[0].text, row.cells[1].text, row.cells[2].text])
                if doc_type == 'urs':
                    writer.writerow(['end of table', 'end of table'])
                else:
                    writer.writerow(['end of table', 'end of table', 'end of table'])
                print(count)
                count+=1'''

    # dfs=read_docx_tables(file_path)
    dfs = make_dataframes(file_path, doc_type, table_pick_criteria)
    # for i in range(len(dfs)):
    # print(dfs[i])
    if doc_type == 'urs':
        df = pd.DataFrame({'Req. Num.': [], 'Original Req. Desc.': []})
        d = {'Req. Num.': ['end of table'], 'Original Req. Desc.': ['end of table']}
    elif doc_type == 'frs':
        df = pd.DataFrame({'FRS Identifier': [], 'Original Req. Desc.': [], 'URS Identifier': []})
        d = {'FRS Identifier': ['end of table'], 'Original Req. Desc.': ['end of table'],
             'URS Identifier': ['end of table']}
    else:
        df = pd.DataFrame({'CS Identifier': [], 'Original Config. Design Specification': [], 'URS/FRS Identifier': []})
        d = {'CS Identifier': ['end of table'], 'Original Config. Design Specification': ['end of table'],
             'URS/FRS Identifier': ['end of table']}

    df_eof = pd.DataFrame(data=d)
    for i in range(0, len(dfs)):
        if (doc_type != 'ds' and dfs[i].columns[0].strip() == table_pick_criteria) or (
                doc_type == 'ds' and dfs[i].columns[0].strip().endswith(table_pick_criteria)):
            if doc_type == 'urs':
                dfs[i].columns = ['Req. Num.', 'Original Req. Desc.', 'Risk Determination', 'Risk Level']
                df = df.append(dfs[i].iloc[:, [0, 1]])
                dfs[i].iloc[:, [0, 1]].to_csv("media/" + doc_type + "/" + name + '.csv', encoding='utf-8', mode='a',
                                              header=False, index=False)
            elif doc_type == 'frs':
                dfs[i].columns = ['FRS Identifier', 'Original Req. Desc.', 'URS Identifier']
                df = df.append(dfs[i].iloc[:, [0, 1, 2]])
                dfs[i].iloc[:, [0, 1, 2]].to_csv("media/" + doc_type + "/" + name + '.csv', encoding='utf-8', mode='a',
                                                 header=False, index=False)
            else:
                dfs[i].columns = ['CS Identifier', 'Original Config. Design Specification', 'URS/FRS Identifier']
                df = df.append(dfs[i].iloc[:, [0, 1, 2]])
                dfs[i].iloc[:, [0, 1, 2]].to_csv("media/" + doc_type + "/" + name + '.csv', encoding='utf-8', mode='a',
                                                 header=False, index=False)
            df_eof.to_csv("media/" + doc_type + "/" + name + '.csv', encoding='utf-8', mode='a', header=False,
                          index=False)

    # write first 2 columns to the excel file
    '''if doc_type == 'urs':
        data = {'Req. Num.': col1, 'Original Req. Desc.': col2}
        df = pd.DataFrame(data, columns=['Req. Num.', 'Original Req. Desc.'])
        df.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=False)
    elif doc_type == 'frs':
        data = {'FRS Identifier': col1, 'Original Req. Desc.': col2, 'URS Identifier': col3}
        df = pd.DataFrame(data, columns=['FRS Identifier', 'Original Req. Desc.', 'URS Identifier'])
        df.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=False)
    else:
        data = {'CS Identifier': col1, 'Original Config. Design Specification': col2, 'URS/FRS Identifier': col3}
        df = pd.DataFrame(data, columns=['CS Identifier', 'Original Config. Design Specification', 'URS/FRS Identifier'])
        df.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=False)
    '''
    df.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=False)

    '''if len(col1)==0:
        messages.error(request, 'File not processed, please check Table Identifier')'''


# If uploaded urs file is CSV type take the first two columns(requirement number and req description)
# of required tables in it and put it in another csv file
def write_csv_to_csv(file_path):
    with open(file_path, 'r', newline='') as f, open("media/" + name + '.csv', 'w', encoding='utf-8',
                                                     newline='') as f_out:
        reader = csv.reader(f)
        writer = csv.writer(f_out)
        for row in reader:
            writer.writerow([row[0]])
            writer.writerow([row[1]])

    # -----------not implemented ---------------


# Find Unique errors
def check_unique_errors(context, doc_type):
    reqlist = []
    unique_error_list = []
    with open("media/" + doc_type + "/" + name + '.csv', 'r', encoding='utf-8') as file:
        csv_file_reader = csv.reader(file)
        for row in csv_file_reader:

            # ignore if the cell contains N/A or end of table
            if row[0] == "end of table":
                continue
            # check if the root is in valid identifiers list
            # elif temp in reqset:
            # unique_error_list.append(temp)
            else:
                temp = remove_whitespace(row[0])
                reqlist.append(temp)

    d = Counter(reqlist)
    with open("media/" + doc_type + "/" + name + '.csv', 'r', encoding='utf-8') as file1:
        csv_file = csv.reader(file1)
        for row in csv_file:
            # ignore if the cell contains N/A or end of table
            if row[0] == "end of table":
                continue
            # check if the root is in valid identifiers list
            elif d[remove_whitespace(row[0])] > 1:
                unique_error_list.append(row[0])

    context['len_unique_error_list'] = len(unique_error_list)
    context['unique_error_list'] = unique_error_list
    return unique_error_list


# Find Grammar errors in description
def check_desc_errors(context, doc_type):
    desc_error_list = []
    desc_errors = []
    completelist = []
    corrections_list = []
    system_shall_have_list = []
    original_but_incorrect_list = []
    with open("media/" + doc_type + "/" + name + '.csv', 'r', encoding='utf-8') as file:
        csv_file_reader = csv.reader(file)
        list1 = []
        list2 = []
        for row in csv_file_reader:
            temp_id = row[0]
            temp = row[1]
            root = 'The system shall'
            # ignore if the cell contains N/A or end of table
            if temp == "end of table":
                continue
            # check if the desc starts with the root
            elif not (temp.startswith(root)):
                if doc_type == 'urs' or doc_type == 'frs':
                    system_shall_have_list.append("Description must start with \'The system shall\'")
                    desc_error_list.append((temp_id, temp))
                    desc_errors.append(temp)
            else:
                system_shall_have_list.append("*Format OK*")

            # check grammar using GingerIt
            # text = temp
            # parser = GingerIt()
            # result = parser.parse(text)

            # temporary = []
            # # If there is grammatical error
            # if text != result['result']:
            #     for x in result['corrections']:
            #         y = x['text'] + '-->' + x['correct']
            #         temporary.append(y)
            #     completelist.append(result['result'])
            #     list1.append(result['result'])
            #     list2.append((temp_id, temp))
            #     original_but_incorrect_list.append(temp)
            # else:
            #     temporary.append('*No Correction*')
            #     completelist.append('*No Correction*')
            # corrections_list.append(temporary)
            temporary = []
            matches = tool.check(temp)
            # print(str(len(matches)))
            import threading
            # logger.debug("temp starts")

            # import Queue
            # que = Queue.Queue()
            # t = threading.Thread(target= lambda q, arg1: q.put(tool.check(arg1)), args=temp)
            # t.start()
            # t.join()
            # matches=que.get()

            if (len(matches) == 0):
                temporary.append('*No spell/grammar correction*')
                completelist.append('*No spell/grammar correction*')
            else:
                for i in range(len(matches)):
                    y = ''.join(matches[i].msg) + '-->' + ','.join(matches[i].replacements)
                    temporary.append(y)
                z = language_check.correct(temp, matches)
                # t1 = threading.Thread(name="non-daemon", target=language_check.correct, args=(temp,matches))
                # t1.setDaemon(True)
                # t1.start()
                completelist.append(z)
                list1.append(z)
                list2.append((temp_id, temp))
                original_but_incorrect_list.append(temp)

            corrections_list.append(temporary)

        correction_list = zip(list1, list2)
        temp_list = zip(list2, list1)
        # write sugggested changes to a new csv file
        with open("media/" + doc_type + "/" + name + 'updated.csv', 'w', encoding='utf-8', newline='') as f_out:
            writer = csv.writer(f_out)
            writer.writerow(['Source Statement', 'Suggested Statement'])
            writer.writerows(temp_list)

    context['correction_list'] = correction_list
    context['len_correction_list'] = len(list1)
    context['len_desc_error_list'] = len(desc_error_list)
    context['desc_error_list'] = desc_error_list

    # Add corrections to excel file
    df = pd.read_excel("media/" + doc_type + "/" + name + '.xlsx', na_filter=False)  # Read Excel file as a DataFrame
    if doc_type == 'urs' or doc_type == 'frs':
        df['Format error - \'The system shall missing\''] = system_shall_have_list
    df['Suggested Req. Desc. based on Grammar and Spell check'] = completelist
    df['Grammar and Spell Corrections'] = corrections_list
    df.to_excel("media/" + doc_type + "/" + name + '.xlsx', index=None)

    fs = FileSystemStorage(location="media/" + doc_type + "/", base_url="/media/" + doc_type + "/")
    context['report_url'] = fs.url(name + 'updated.csv')
    return desc_errors, original_but_incorrect_list


# Check identifiers mistype error
def check_id_errors(context, doc_type):
    if doc_type == 'urs':
        identifiers = ['GL-U-SF-PM-', 'GL-U-SF-DC-', 'GL-U-SF-PT-', 'GL-U-SF-NC-', 'GL-U-SF-DOC-', 'GL-U-SF-SYS-',
                       'GL-U-RG-', 'GL-U-INT-', 'GL-U-SF-REP-', 'GL-U-UR-']
    elif doc_type == 'frs':
        identifiers = ['GL-F-SF-PM-', 'GL-F-SF-DC-', 'GL-F-SF-PT-', 'GL-F-SF-NC-', 'GL-F-SF-DOC-', 'GL-F-SF-SYS-',
                       'GL-F-RG-', 'GL-F-INT-', 'GL-F-SF-REP-', 'GL-F-UR-']
    else:
        identifiers = ['GL-U-SF-PM-', 'GL-U-SF-DC-', 'GL-U-SF-PT-', 'GL-U-SF-NC-', 'GL-U-SF-DOC-', 'GL-U-SF-SYS-',
                       'GL-U-RG-', 'GL-U-INT-', 'GL-U-SF-REP-', 'GL-U-UR-']
    identifier_error_list = []
    with open("media/" + doc_type + "/" + name + '.csv', 'r', encoding='utf-8') as file:
        csv_file_reader = csv.reader(file)
        for row in csv_file_reader:
            temp = row[0]

            # ignore if the cell contains N/A or end of table
            if temp == "end of table":
                continue
            # check if the root is in valid identifiers list
            else:
                if doc_type == 'urs':
                    if (temp.rsplit('-', 1)[0] + '-' not in identifiers):
                        if (temp != ""):
                            error = temp
                        else:
                            error = "Req. Num field empty for -----> " + row[1]
                        identifier_error_list.append(error)
                elif doc_type == 'frs':
                    if (temp.rsplit('-', 2)[0] + '-' not in identifiers):
                        if (temp != ""):
                            error = temp
                        else:
                            error = "Req. Num field empty for -----> " + row[1]
                        identifier_error_list.append(error)

    context['len_identifier_error_list'] = len(identifier_error_list)
    context['identifier_error_list'] = identifier_error_list
    return identifier_error_list


# Deep Learnig Model
def DeepLearning(train_data, query):
    # import libraries
    import warnings
    warnings.filterwarnings("ignore")
    from keras.preprocessing.text import Tokenizer
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.layers import LSTM
    from keras.layers import Embedding
    result = []
    train_data = clean_document(train_data)
    # use Keras' Tokenizer() function to encode text to integers
    word_tokeniser = Tokenizer()
    word_tokeniser.fit_on_texts([train_data])
    encoded_words = word_tokeniser.texts_to_sequences([train_data])[0]
    # check the size of the vocabulary
    VOCABULARY_SIZE = len(word_tokeniser.word_index) + 1
    sequences = []
    MAX_SEQ_LENGTH = 5  # X will have five words, y will have the sixth word
    # create model architecture
    EMBEDDING_SIZE = 100
    model = Sequential()
    # embedding layer
    model.add(Embedding(VOCABULARY_SIZE, EMBEDDING_SIZE, input_length=MAX_SEQ_LENGTH))
    # lstm layer 1
    model.add(LSTM(128, return_sequences=True))
    # lstm layer 2
    model.add(LSTM(128))
    # output layer
    model.add(Dense(VOCABULARY_SIZE, activation='softmax'))
    # load the network weights
    filename = 'static/assets/model-00010-0.44450.h5'
    model.load_weights(filename)
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    seed_text = "electronic records"
    num_words = 25
    for line in query:
        line = "The system shall have" + " " + line
        result.append(generate_words(model, word_tokeniser, MAX_SEQ_LENGTH, line, num_words))
    return result


# Generate sentence using the Model
def generate_words(model, word_tokeniser, MAX_SEQ_LENGTH, seed, n_words):
    from keras.preprocessing.sequence import pad_sequences
    text = seed
    new_text = ""

    start_list = ["the system shall"]
    # generate n_words
    for _ in range(n_words):
        # encode text as integers
        encoded_words = word_tokeniser.texts_to_sequences([text])[0]

        # pad sequences
        padded_words = pad_sequences([encoded_words], maxlen=MAX_SEQ_LENGTH, padding='pre')

        # predict next word
        prediction = model.predict_classes(padded_words, verbose=0)

        # convert predicted index to its word
        next_word = ""
        for word, i in word_tokeniser.word_index.items():
            if i == prediction:
                next_word = word
                break

        # append predicted word to text
        text += " " + next_word
        last_three_words_list = text.split()[-3:]
        last_three_words = ' '.join([str(elem) for elem in last_three_words_list])
        if last_three_words in start_list:
            new_text = text.rsplit(" ", 3)[0]
            return new_text

    # matches = tool.check(text)
    # return language_check.correct(text, matches)
    return text


# Function to clean document
def clean_document(document, char_filter=r"[^\w]"):
    import re
    # convert words to lower case
    document = document.lower()
    # tokenise words
    words = word_tokenize(document)
    # strip whitespace from all words
    words = [word.strip() for word in words]
    # join back words to get document
    document = " ".join(words)
    # remove unwanted characters
    document = re.sub(char_filter, " ", document)
    # replace multiple whitespaces with single whitespace
    document = re.sub(r"\s+", " ", document)
    # strip whitespace from document
    document = document.strip()
    return document


# View for uploading FRS docx file to check grammar and create a excel report
def upload_frs(request):
    doc_type = 'frs'
    context = {}
    global name

    # Check if uploaded file is docx/csv
    if request.method == 'POST' and 'upload_frs' in request.POST:
        table_identifier = request.POST.get('table_identifier', None)
        uploaded_frs_file = request.FILES['FRS document']
        name = check_csv_or_docx(uploaded_frs_file, request, context, doc_type)
        request.session['name'] = name

        ext = os.path.splitext(uploaded_frs_file.name)[1]
        # if file is docx
        if ext == ".docx":
            file_path = "media/" + doc_type + "/" + name
            write_docx_to_csv(file_path, request, doc_type, table_identifier)

        # if file is a csv type
        elif ext == ".csv":
            file_path = "media/" + name
            write_csv_to_csv(file_path)

    # Validation Rules
    if request.method == 'POST' and 'validate_frs' in request.POST:
        coloring_list = []
        name = request.session['name']
        empty_list = ['', 'nan', 'NaN', 'Nan', 'naN']
        unique_error_list = check_unique_errors(context, doc_type)
        desc_errors, original_but_incorrect_list = check_desc_errors(context, doc_type)
        identifier_error_list = check_id_errors(context, doc_type)

        coloring_list.append(empty_list)
        coloring_list.append(unique_error_list)
        coloring_list.append(desc_errors)
        coloring_list.append(original_but_incorrect_list)
        coloring_list.append(identifier_error_list)

        applycolor(coloring_list, doc_type)

        fs = FileSystemStorage(location="media/", base_url="/media/" + doc_type + "/")
        context['full_report_url'] = fs.url(name + '.xlsx')
        return render(request, 'validate_frs.html', context)

    return render(request, 'upload_frs.html', context)


# View for uploading DS docx file to check grammar and create a excel report
def upload_ds(request):
    doc_type = 'ds'
    context = {}
    global name

    # Check if uploaded file is docx/csv
    if request.method == 'POST' and 'upload_ds' in request.POST:
        table_identifier = 'Identifier'
        uploaded_ds_file = request.FILES['DS document']
        name = check_csv_or_docx(uploaded_ds_file, request, context, doc_type)
        request.session['name'] = name

        ext = os.path.splitext(uploaded_ds_file.name)[1]
        # if file is docx
        if ext == ".docx":
            file_path = "media/" + doc_type + "/" + name
            write_docx_to_csv(file_path, request, doc_type, table_identifier)

        # if file is a csv type
        elif ext == ".csv":
            file_path = "media/" + name
            write_csv_to_csv(file_path)

    # Validation Rules
    if request.method == 'POST' and 'validate_ds' in request.POST:
        coloring_list = []
        name = request.session['name']
        empty_list = ['', 'nan', 'NaN', 'Nan', 'naN']
        unique_error_list = check_unique_errors(context, doc_type)
        desc_errors, original_but_incorrect_list = check_desc_errors(context, doc_type)
        identifier_error_list = check_id_errors(context, doc_type)

        coloring_list.append(empty_list)
        coloring_list.append(unique_error_list)
        coloring_list.append(desc_errors)
        coloring_list.append(original_but_incorrect_list)
        coloring_list.append(identifier_error_list)

        applycolor(coloring_list, doc_type)

        fs = FileSystemStorage(location="media/", base_url="/media/" + doc_type + "/")
        context['full_report_url'] = fs.url(name + '.xlsx')
        return render(request, 'validate_ds.html', context)

    return render(request, 'upload_ds.html', context)
